class Code
  attr_reader :pegs

  PEGS = { 'r' => "red", 'o' => "orange", 'y' => "yellow", 'g' => "green", 'b' => "blue", 'p' => "purple" }

  def initialize(pegs)
    @pegs = pegs
  end

  def self.parse(str)
    @parse = str.downcase
    pegs = @parse.chars.map{|x|
      raise 'error' if PEGS[x.downcase] == nil
      PEGS[x]
    }
    Code.new(pegs)
  end

  def self.random
    values = PEGS.values
    code = values.shuffle.take(4)
    Code.new(code)
  end

  def [](index)
    return @pegs[index]
  end

  def exact_matches(code)
    matches = 0
    pegs.each_with_index do |x, i|
      matches += 1 if code[i] == pegs[i]
    end
    matches
  end

  def near_matches(code)
    near_matches = []
    code.pegs.each_with_index do |peg, i|
      if code[i] != pegs[i] && pegs.include?(peg)
        near_matches << peg
      end
    end
    p code
    near_matches.uniq.length
  end

  def ==(other_code)
   return false unless other_code.is_a?(Code)
   first_pegs = self.pegs.map{|x| x.downcase unless x == nil}
   second_pegs = other_code.pegs.map{|x| x.downcase unless x == nil}
   return true if first_pegs == second_pegs
  end
end

class Game
  attr_reader :secret_code

  def initialize(secret_code = Code.random)
    @secret_code = secret_code
  end

  def get_guess
    puts "Guess a code"
    user_guess = Code.parse($stdin.gets.chomp)
  end

  def display_matches(guess)
    exact_matches = @secret_code.exact_matches(guess)
    near_matches = @secret_code.near_matches(guess)
    puts "There are #{exact_matches} exact matches"
    puts "There are #{near_matches} near matches"
  end
end
